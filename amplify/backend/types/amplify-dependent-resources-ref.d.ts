export type AmplifyDependentResourcesAttributes = {
  "api": {
    "budgetapp": {
      "GraphQLAPIEndpointOutput": "string",
      "GraphQLAPIIdOutput": "string"
    }
  },
  "auth": {
    "budgetapp04b12c2e": {
      "AppClientID": "string",
      "AppClientIDWeb": "string",
      "IdentityPoolId": "string",
      "IdentityPoolName": "string",
      "UserPoolArn": "string",
      "UserPoolId": "string",
      "UserPoolName": "string"
    }
  },
  "storage": {
    "s336e199f5": {
      "BucketName": "string",
      "Region": "string"
    }
  }
}