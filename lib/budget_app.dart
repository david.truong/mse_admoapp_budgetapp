import 'package:amplify_authenticator/amplify_authenticator.dart';
import 'package:budget_app/common/navigation/router/router.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

// class LocalizedButtonResolver extends ButtonResolver {}

class BudgetApp extends StatelessWidget {
  const BudgetApp({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Authenticator(
      child: MaterialApp.router(
          title: 'Amplify Budget Planner',
          localizationsDelegates: const [
            AppLocalizations.delegate,
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
          ],
          supportedLocales: const [
            Locale('en'),
            Locale('fr'),
            Locale('de'),
            Locale('it')
          ],
          routerConfig: router,
          builder: Authenticator.builder(),
          theme: ThemeData.dark(useMaterial3: true)),
    );
  }
}
