/*
* Copyright 2021 Amazon.com, Inc. or its affiliates. All Rights Reserved.
*
* Licensed under the Apache License, Version 2.0 (the "License").
* You may not use this file except in compliance with the License.
* A copy of the License is located at
*
*  http://aws.amazon.com/apache2.0
*
* or in the "license" file accompanying this file. This file is distributed
* on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied. See the License for the specific language governing
* permissions and limitations under the License.
*/

// NOTE: This file is generated and may not follow lint rules defined in your app
// Generated files can be excluded from analysis in analysis_options.yaml
// For more info, see: https://dart.dev/guides/language/analysis-options#excluding-code-from-analysis

// ignore_for_file: public_member_api_docs, annotate_overrides, dead_code, dead_codepublic_member_api_docs, depend_on_referenced_packages, file_names, library_private_types_in_public_api, no_leading_underscores_for_library_prefixes, no_leading_underscores_for_local_identifiers, non_constant_identifier_names, null_check_on_nullable_type_parameter, prefer_adjacent_string_concatenation, prefer_const_constructors, prefer_if_null_operators, prefer_interpolation_to_compose_strings, slash_for_doc_comments, sort_child_properties_last, unnecessary_const, unnecessary_constructor_name, unnecessary_late, unnecessary_new, unnecessary_null_aware_assignments, unnecessary_nullable_for_final_variable_declarations, unnecessary_string_interpolations, use_build_context_synchronously

import 'ModelProvider.dart';
import 'package:amplify_core/amplify_core.dart' as amplify_core;
import 'package:collection/collection.dart';

/** This is an auto generated class representing the Budget type in your schema. */
class Budget extends amplify_core.Model {
  static const classType = const _BudgetModelType();
  final String id;
  final String? _budgetName;
  final String? _destination;
  final amplify_core.TemporalDate? _startDate;
  final amplify_core.TemporalDate? _endDate;
  final int? _limit;
  final String? _budgetImageUrl;
  final String? _budgetImageKey;
  final List<Transaction>? _RelBudgetTransaction;
  final amplify_core.TemporalDateTime? _createdAt;
  final amplify_core.TemporalDateTime? _updatedAt;

  @override
  getInstanceType() => classType;

  @Deprecated(
      '[getId] is being deprecated in favor of custom primary key feature. Use getter [modelIdentifier] to get model identifier.')
  @override
  String getId() => id;

  BudgetModelIdentifier get modelIdentifier {
    return BudgetModelIdentifier(id: id);
  }

  String get budgetName {
    try {
      return _budgetName!;
    } catch (e) {
      throw amplify_core.AmplifyCodeGenModelException(
          amplify_core.AmplifyExceptionMessages
              .codeGenRequiredFieldForceCastExceptionMessage,
          recoverySuggestion: amplify_core.AmplifyExceptionMessages
              .codeGenRequiredFieldForceCastRecoverySuggestion,
          underlyingException: e.toString());
    }
  }

  String get destination {
    try {
      return _destination!;
    } catch (e) {
      throw amplify_core.AmplifyCodeGenModelException(
          amplify_core.AmplifyExceptionMessages
              .codeGenRequiredFieldForceCastExceptionMessage,
          recoverySuggestion: amplify_core.AmplifyExceptionMessages
              .codeGenRequiredFieldForceCastRecoverySuggestion,
          underlyingException: e.toString());
    }
  }

  amplify_core.TemporalDate get startDate {
    try {
      return _startDate!;
    } catch (e) {
      throw amplify_core.AmplifyCodeGenModelException(
          amplify_core.AmplifyExceptionMessages
              .codeGenRequiredFieldForceCastExceptionMessage,
          recoverySuggestion: amplify_core.AmplifyExceptionMessages
              .codeGenRequiredFieldForceCastRecoverySuggestion,
          underlyingException: e.toString());
    }
  }

  amplify_core.TemporalDate get endDate {
    try {
      return _endDate!;
    } catch (e) {
      throw amplify_core.AmplifyCodeGenModelException(
          amplify_core.AmplifyExceptionMessages
              .codeGenRequiredFieldForceCastExceptionMessage,
          recoverySuggestion: amplify_core.AmplifyExceptionMessages
              .codeGenRequiredFieldForceCastRecoverySuggestion,
          underlyingException: e.toString());
    }
  }

  int? get limit {
    return _limit;
  }

  String? get budgetImageUrl {
    return _budgetImageUrl;
  }

  String? get budgetImageKey {
    return _budgetImageKey;
  }

  List<Transaction>? get RelBudgetTransaction {
    return _RelBudgetTransaction;
  }

  amplify_core.TemporalDateTime? get createdAt {
    return _createdAt;
  }

  amplify_core.TemporalDateTime? get updatedAt {
    return _updatedAt;
  }

  const Budget._internal(
      {required this.id,
      required budgetName,
      required destination,
      required startDate,
      required endDate,
      limit,
      budgetImageUrl,
      budgetImageKey,
      RelBudgetTransaction,
      createdAt,
      updatedAt})
      : _budgetName = budgetName,
        _destination = destination,
        _startDate = startDate,
        _endDate = endDate,
        _limit = limit,
        _budgetImageUrl = budgetImageUrl,
        _budgetImageKey = budgetImageKey,
        _RelBudgetTransaction = RelBudgetTransaction,
        _createdAt = createdAt,
        _updatedAt = updatedAt;

  factory Budget(
      {String? id,
      required String budgetName,
      required String destination,
      required amplify_core.TemporalDate startDate,
      required amplify_core.TemporalDate endDate,
      int? limit,
      String? budgetImageUrl,
      String? budgetImageKey,
      List<Transaction>? RelBudgetTransaction}) {
    return Budget._internal(
        id: id == null ? amplify_core.UUID.getUUID() : id,
        budgetName: budgetName,
        destination: destination,
        startDate: startDate,
        endDate: endDate,
        limit: limit,
        budgetImageUrl: budgetImageUrl,
        budgetImageKey: budgetImageKey,
        RelBudgetTransaction: RelBudgetTransaction != null
            ? List<Transaction>.unmodifiable(RelBudgetTransaction)
            : RelBudgetTransaction);
  }

  bool equals(Object other) {
    return this == other;
  }

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Budget &&
        id == other.id &&
        _budgetName == other._budgetName &&
        _destination == other._destination &&
        _startDate == other._startDate &&
        _endDate == other._endDate &&
        _limit == other._limit &&
        _budgetImageUrl == other._budgetImageUrl &&
        _budgetImageKey == other._budgetImageKey &&
        DeepCollectionEquality()
            .equals(_RelBudgetTransaction, other._RelBudgetTransaction);
  }

  @override
  int get hashCode => toString().hashCode;

  @override
  String toString() {
    var buffer = new StringBuffer();

    buffer.write("Budget {");
    buffer.write("id=" + "$id" + ", ");
    buffer.write("budgetName=" + "$_budgetName" + ", ");
    buffer.write("destination=" + "$_destination" + ", ");
    buffer.write("startDate=" +
        (_startDate != null ? _startDate!.format() : "null") +
        ", ");
    buffer.write(
        "endDate=" + (_endDate != null ? _endDate!.format() : "null") + ", ");
    buffer.write(
        "limit=" + (_limit != null ? _limit!.toString() : "null") + ", ");
    buffer.write("budgetImageUrl=" + "$_budgetImageUrl" + ", ");
    buffer.write("budgetImageKey=" + "$_budgetImageKey" + ", ");
    buffer.write("createdAt=" +
        (_createdAt != null ? _createdAt!.format() : "null") +
        ", ");
    buffer.write(
        "updatedAt=" + (_updatedAt != null ? _updatedAt!.format() : "null"));
    buffer.write("}");

    return buffer.toString();
  }

  Budget copyWith(
      {String? budgetName,
      String? destination,
      amplify_core.TemporalDate? startDate,
      amplify_core.TemporalDate? endDate,
      int? limit,
      String? budgetImageUrl,
      String? budgetImageKey,
      List<Transaction>? RelBudgetTransaction}) {
    return Budget._internal(
        id: id,
        budgetName: budgetName ?? this.budgetName,
        destination: destination ?? this.destination,
        startDate: startDate ?? this.startDate,
        endDate: endDate ?? this.endDate,
        limit: limit ?? this.limit,
        budgetImageUrl: budgetImageUrl ?? this.budgetImageUrl,
        budgetImageKey: budgetImageKey ?? this.budgetImageKey,
        RelBudgetTransaction:
            RelBudgetTransaction ?? this.RelBudgetTransaction);
  }

  Budget copyWithModelFieldValues(
      {ModelFieldValue<String>? budgetName,
      ModelFieldValue<String>? destination,
      ModelFieldValue<amplify_core.TemporalDate>? startDate,
      ModelFieldValue<amplify_core.TemporalDate>? endDate,
      ModelFieldValue<int?>? limit,
      ModelFieldValue<String?>? budgetImageUrl,
      ModelFieldValue<String?>? budgetImageKey,
      ModelFieldValue<List<Transaction>?>? RelBudgetTransaction}) {
    return Budget._internal(
        id: id,
        budgetName: budgetName == null ? this.budgetName : budgetName.value,
        destination: destination == null ? this.destination : destination.value,
        startDate: startDate == null ? this.startDate : startDate.value,
        endDate: endDate == null ? this.endDate : endDate.value,
        limit: limit == null ? this.limit : limit.value,
        budgetImageUrl:
            budgetImageUrl == null ? this.budgetImageUrl : budgetImageUrl.value,
        budgetImageKey:
            budgetImageKey == null ? this.budgetImageKey : budgetImageKey.value,
        RelBudgetTransaction: RelBudgetTransaction == null
            ? this.RelBudgetTransaction
            : RelBudgetTransaction.value);
  }

  Budget.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        _budgetName = json['budgetName'],
        _destination = json['destination'],
        _startDate = json['startDate'] != null
            ? amplify_core.TemporalDate.fromString(json['startDate'])
            : null,
        _endDate = json['endDate'] != null
            ? amplify_core.TemporalDate.fromString(json['endDate'])
            : null,
        _limit = (json['limit'] as num?)?.toInt(),
        _budgetImageUrl = json['budgetImageUrl'],
        _budgetImageKey = json['budgetImageKey'],
        _RelBudgetTransaction = json['RelBudgetTransaction'] is List
            ? (json['RelBudgetTransaction'] as List)
                .where((e) => e?['serializedData'] != null)
                .map((e) => Transaction.fromJson(
                    new Map<String, dynamic>.from(e['serializedData'])))
                .toList()
            : null,
        _createdAt = json['createdAt'] != null
            ? amplify_core.TemporalDateTime.fromString(json['createdAt'])
            : null,
        _updatedAt = json['updatedAt'] != null
            ? amplify_core.TemporalDateTime.fromString(json['updatedAt'])
            : null;

  Map<String, dynamic> toJson() => {
        'id': id,
        'budgetName': _budgetName,
        'destination': _destination,
        'startDate': _startDate?.format(),
        'endDate': _endDate?.format(),
        'limit': _limit,
        'budgetImageUrl': _budgetImageUrl,
        'budgetImageKey': _budgetImageKey,
        'RelBudgetTransaction':
            _RelBudgetTransaction?.map((Transaction? e) => e?.toJson())
                .toList(),
        'createdAt': _createdAt?.format(),
        'updatedAt': _updatedAt?.format()
      };

  Map<String, Object?> toMap() => {
        'id': id,
        'budgetName': _budgetName,
        'destination': _destination,
        'startDate': _startDate,
        'endDate': _endDate,
        'limit': _limit,
        'budgetImageUrl': _budgetImageUrl,
        'budgetImageKey': _budgetImageKey,
        'RelBudgetTransaction': _RelBudgetTransaction,
        'createdAt': _createdAt,
        'updatedAt': _updatedAt
      };

  static final amplify_core.QueryModelIdentifier<BudgetModelIdentifier>
      MODEL_IDENTIFIER =
      amplify_core.QueryModelIdentifier<BudgetModelIdentifier>();
  static final ID = amplify_core.QueryField(fieldName: "id");
  static final BUDGETNAME = amplify_core.QueryField(fieldName: "budgetName");
  static final DESTINATION = amplify_core.QueryField(fieldName: "destination");
  static final STARTDATE = amplify_core.QueryField(fieldName: "startDate");
  static final ENDDATE = amplify_core.QueryField(fieldName: "endDate");
  static final LIMIT = amplify_core.QueryField(fieldName: "limit");
  static final BUDGETIMAGEURL =
      amplify_core.QueryField(fieldName: "budgetImageUrl");
  static final BUDGETIMAGEKEY =
      amplify_core.QueryField(fieldName: "budgetImageKey");
  static final RELBUDGETTRANSACTION = amplify_core.QueryField(
      fieldName: "RelBudgetTransaction",
      fieldType: amplify_core.ModelFieldType(
          amplify_core.ModelFieldTypeEnum.model,
          ofModelName: 'Transaction'));
  static var schema = amplify_core.Model.defineSchema(
      define: (amplify_core.ModelSchemaDefinition modelSchemaDefinition) {
    modelSchemaDefinition.name = "Budget";
    modelSchemaDefinition.pluralName = "Budgets";

    modelSchemaDefinition.authRules = [
      amplify_core.AuthRule(
          authStrategy: amplify_core.AuthStrategy.OWNER,
          ownerField: "owner",
          identityClaim: "cognito:username",
          provider: amplify_core.AuthRuleProvider.USERPOOLS,
          operations: const [
            amplify_core.ModelOperation.CREATE,
            amplify_core.ModelOperation.UPDATE,
            amplify_core.ModelOperation.DELETE,
            amplify_core.ModelOperation.READ
          ])
    ];

    modelSchemaDefinition.addField(amplify_core.ModelFieldDefinition.id());

    modelSchemaDefinition.addField(amplify_core.ModelFieldDefinition.field(
        key: Budget.BUDGETNAME,
        isRequired: true,
        ofType: amplify_core.ModelFieldType(
            amplify_core.ModelFieldTypeEnum.string)));

    modelSchemaDefinition.addField(amplify_core.ModelFieldDefinition.field(
        key: Budget.DESTINATION,
        isRequired: true,
        ofType: amplify_core.ModelFieldType(
            amplify_core.ModelFieldTypeEnum.string)));

    modelSchemaDefinition.addField(amplify_core.ModelFieldDefinition.field(
        key: Budget.STARTDATE,
        isRequired: true,
        ofType:
            amplify_core.ModelFieldType(amplify_core.ModelFieldTypeEnum.date)));

    modelSchemaDefinition.addField(amplify_core.ModelFieldDefinition.field(
        key: Budget.ENDDATE,
        isRequired: true,
        ofType:
            amplify_core.ModelFieldType(amplify_core.ModelFieldTypeEnum.date)));

    modelSchemaDefinition.addField(amplify_core.ModelFieldDefinition.field(
        key: Budget.LIMIT,
        isRequired: false,
        ofType:
            amplify_core.ModelFieldType(amplify_core.ModelFieldTypeEnum.int)));

    modelSchemaDefinition.addField(amplify_core.ModelFieldDefinition.field(
        key: Budget.BUDGETIMAGEURL,
        isRequired: false,
        ofType: amplify_core.ModelFieldType(
            amplify_core.ModelFieldTypeEnum.string)));

    modelSchemaDefinition.addField(amplify_core.ModelFieldDefinition.field(
        key: Budget.BUDGETIMAGEKEY,
        isRequired: false,
        ofType: amplify_core.ModelFieldType(
            amplify_core.ModelFieldTypeEnum.string)));

    modelSchemaDefinition.addField(amplify_core.ModelFieldDefinition.hasMany(
        key: Budget.RELBUDGETTRANSACTION,
        isRequired: false,
        ofModelName: 'Transaction',
        associatedKey: Transaction.BUDGETID));

    modelSchemaDefinition.addField(
        amplify_core.ModelFieldDefinition.nonQueryField(
            fieldName: 'createdAt',
            isRequired: false,
            isReadOnly: true,
            ofType: amplify_core.ModelFieldType(
                amplify_core.ModelFieldTypeEnum.dateTime)));

    modelSchemaDefinition.addField(
        amplify_core.ModelFieldDefinition.nonQueryField(
            fieldName: 'updatedAt',
            isRequired: false,
            isReadOnly: true,
            ofType: amplify_core.ModelFieldType(
                amplify_core.ModelFieldTypeEnum.dateTime)));
  });
}

class _BudgetModelType extends amplify_core.ModelType<Budget> {
  const _BudgetModelType();

  @override
  Budget fromJson(Map<String, dynamic> jsonData) {
    return Budget.fromJson(jsonData);
  }

  @override
  String modelName() {
    return 'Budget';
  }
}

/**
 * This is an auto generated class representing the model identifier
 * of [Budget] in your schema.
 */
class BudgetModelIdentifier implements amplify_core.ModelIdentifier<Budget> {
  final String id;

  /** Create an instance of BudgetModelIdentifier using [id] the primary key. */
  const BudgetModelIdentifier({required this.id});

  @override
  Map<String, dynamic> serializeAsMap() => (<String, dynamic>{'id': id});

  @override
  List<Map<String, dynamic>> serializeAsList() => serializeAsMap()
      .entries
      .map((entry) => (<String, dynamic>{entry.key: entry.value}))
      .toList();

  @override
  String serializeAsString() => serializeAsMap().values.join('#');

  @override
  String toString() => 'BudgetModelIdentifier(id: $id)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) {
      return true;
    }

    return other is BudgetModelIdentifier && id == other.id;
  }

  @override
  int get hashCode => id.hashCode;
}
