import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:budget_app/common/navigation/router/routes.dart';
import 'package:budget_app/common/ui/bottomsheet_text_form_field.dart';
import 'package:budget_app/common/utils/colors.dart' as constants;
import 'package:budget_app/common/utils/date_time_formatter.dart';
import 'package:budget_app/features/transactions/controller/transaction_controller.dart';
import 'package:budget_app/models/ModelProvider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class EditTransactionPage extends ConsumerStatefulWidget {
  const EditTransactionPage({
    required this.transaction,
    super.key,
  });

  final Transaction transaction;

  @override
  EditBudgetPageState createState() => EditBudgetPageState();
}

class EditBudgetPageState extends ConsumerState<EditTransactionPage> {
  @override
  void initState() {
    transactionNameController.text = widget.transaction.name;
    transactionAmountController.text = widget.transaction.amount.toString();
    transactionCurrencyController.text = widget.transaction.currency;
    transactionDateController.text =
        widget.transaction.date.getDateTime().format('yyyy-MM-dd');
    transactionBudgetController.text = widget.transaction.budgetID;

    super.initState();
  }

  final formGlobalKey = GlobalKey<FormState>();
  final transactionNameController = TextEditingController();
  final transactionAmountController = TextEditingController();
  final transactionCurrencyController = TextEditingController();
  final transactionDateController = TextEditingController();
  final transactionBudgetController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          AppLocalizations.of(context)!.appTitle,
        ),
        leading: IconButton(
          onPressed: () {
            context.goNamed(
              AppRoute.transactionList.name,
            );
          },
          icon: const Icon(Icons.arrow_back),
        ),
        backgroundColor: const Color(constants.primaryColorDark),
      ),
      body: SingleChildScrollView(
        child: Form(
          key: formGlobalKey,
          child: Container(
            padding: EdgeInsets.only(
              top: 15,
              left: 15,
              right: 15,
              bottom: MediaQuery.of(context).viewInsets.bottom + 15,
            ),
            width: double.infinity,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                BottomSheetTextFormField(
                  labelText: AppLocalizations.of(context)!.transactionName,
                  controller: transactionNameController,
                  keyboardType: TextInputType.name,
                ),
                const SizedBox(
                  height: 20,
                ),
                BottomSheetTextFormField(
                  labelText: AppLocalizations.of(context)!.transactionAmount,
                  controller: transactionAmountController,
                  keyboardType: TextInputType.name,
                ),
                const SizedBox(
                  height: 20,
                ),
                BottomSheetTextFormField(
                  labelText: AppLocalizations.of(context)!.transactionDate,
                  controller: transactionDateController,
                  keyboardType: TextInputType.datetime,
                  onTap: () async {
                    final pickedDate = await showDatePicker(
                      context: context,
                      initialDate: DateTime.now(),
                      firstDate: DateTime(2000),
                      lastDate: DateTime(2101),
                    );

                    if (pickedDate != null) {
                      transactionDateController.text =
                          pickedDate.format('yyyy-MM-dd');
                    }
                  },
                ),
                const SizedBox(
                  height: 20,
                ),
                TextButton(
                  child: Text(AppLocalizations.of(context)!.updateTransaction),
                  onPressed: () async {
                    final currentState = formGlobalKey.currentState;
                    if (currentState == null) {
                      return;
                    }
                    if (currentState.validate()) {
                      final updatedTransaction = widget.transaction.copyWith(
                          name: transactionNameController.text,
                          amount:
                              double.parse(transactionAmountController.text),
                          currency: transactionCurrencyController.text,
                          date: TemporalDate(
                            DateTime.parse(transactionDateController.text),
                          ),
                          budgetID: transactionBudgetController.text);

                      await ref
                          .watch(transactionControllerProvider(
                                  widget.transaction.id)
                              .notifier)
                          .updateTransaction(updatedTransaction);
                      if (context.mounted) {
                        context.goNamed(
                          AppRoute.transactionList.name,
                        );
                      }
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
