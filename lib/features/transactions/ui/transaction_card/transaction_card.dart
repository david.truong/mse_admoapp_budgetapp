import 'package:budget_app/common/navigation/router/routes.dart';
import 'package:budget_app/features/transactions/controller/transactions_list_controller.dart';
import 'package:budget_app/features/budgets/ui/budget_page/delete_budget_dialog.dart';
import 'package:budget_app/models/Transaction.dart';
import 'package:budget_app/models/ModelProvider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:budget_app/common/utils/colors.dart' as constants;

class TransactionCard extends ConsumerWidget {
  const TransactionCard({
    required this.transaction,
    super.key,
  });

  final Transaction transaction;

  Future<bool> deleteTransaction(
    BuildContext context,
    WidgetRef ref,
    Transaction transaction,
  ) async {
    var value = await showDialog<bool>(
      context: context,
      builder: (BuildContext context) {
        return const DeleteBudgetDialog();
      },
    );
    value ??= false;

    if (value) {
      await ref
          .watch(transactionsListControllerProvider.notifier)
          .removeTransaction(transaction);
    }
    return value;
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Card(
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
      elevation: 5,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const SizedBox(
            height: 10,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              const SizedBox(
                width: 15,
              ),
              const Icon(
                Icons.paid,
                color: Color(constants.primaryColorDark),
                size: 30.0,
                semanticLabel: 'Icon transaction',
              ),
              Expanded(
                child: Column(
                  children: [
                    Text(
                      transaction.name,
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const SizedBox(
                      height: 4,
                    ),
                    Text(
                      "${transaction.amount} ${transaction.currency.toUpperCase()}",
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                        fontSize: 30,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const SizedBox(
                      height: 2,
                    ),
                    Text(
                      transaction.date.toString(),
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.normal,
                      ),
                    ),
                  ],
                ),
              ),
              Column(
                children: [
                  IconButton(
                    onPressed: () {
                      context.goNamed(
                        AppRoute.editTransaction.name,
                        pathParameters: {'id': transaction.id},
                        extra: transaction,
                      );
                    },
                    icon: const Icon(Icons.edit),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  IconButton(
                    onPressed: () {
                      deleteTransaction(context, ref, transaction)
                          .then((value) {
                        if (value) {
                          context.goNamed(
                            AppRoute.transactionList.name,
                          );
                        }
                      });
                    },
                    icon: const Icon(Icons.delete),
                  ),
                ],
              ),
            ],
          ),
          const SizedBox(
            height: 4,
          ),
        ],
      ),
    );
  }
}
