import 'package:budget_app/features/transactions/ui/transaction_card/transaction_card.dart';
import 'package:budget_app/models/ModelProvider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class TransactionListCard extends StatelessWidget {
  const TransactionListCard({
    required this.transactionList,
    super.key,
  });

  final List<Transaction> transactionList;

  @override
  Widget build(BuildContext context) {
    return transactionList.isEmpty
        ? Center(
            child: Text(AppLocalizations.of(context)!.noTransaction),
          )
        : ListView.builder(
            itemCount: transactionList.length,
            itemBuilder: (context, index) {
              final transaction = transactionList[index];
              return TransactionCard(transaction: transaction);
            },
          );
  }
}
