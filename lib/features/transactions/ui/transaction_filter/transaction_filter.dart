import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class TransactionFilter extends StatefulWidget {
  final Function(String, dynamic) onFilterChanged;

  const TransactionFilter({super.key, required this.onFilterChanged});

  @override
  _TransactionFilterState createState() => _TransactionFilterState();
}

class _TransactionFilterState extends State<TransactionFilter> {
  bool _showFilters = false;
  bool _showClear = false;
  String? selectedText;
  double? selectedAmount;
  String? selectedDateMin;
  String? selectedDateMax;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        const SizedBox(
          height: 4,
        ),
        Row(
          children: [
            const SizedBox(
              width: 4,
            ),
            ElevatedButton(
              onPressed: () {
                setState(() {
                  _showFilters = !_showFilters;
                });
              },
              child: Row(
                children: [
                  const Icon(Icons.filter_list),
                  const SizedBox(
                    width: 4,
                  ),
                  Text(_showFilters
                      ? AppLocalizations.of(context)!.filterHide
                      : AppLocalizations.of(context)!.filterShow),
                ],
              ),
            ),
            if (_showClear)
              ElevatedButton(
                  onPressed: () {
                    setState(() {
                      selectedText = null;
                      selectedAmount = null;
                      selectedDateMin = null;
                      selectedDateMax = null;
                      _showClear = false;
                      _showFilters = false;
                    });
                    widget.onFilterChanged('text', null);
                    widget.onFilterChanged('amount', null);
                    widget.onFilterChanged('dateMin', null);
                    widget.onFilterChanged('dateMax', null);
                  },
                  child: const Icon(Icons.clear)),
          ],
        ),
        Visibility(
          visible: _showFilters,
          child: Column(
            children: [
              TextField(
                onChanged: (value) {
                  setState(() {
                    selectedText = value;
                    _showClear = true;
                  });
                  widget.onFilterChanged('text', selectedText);
                },
                decoration: InputDecoration(
                  hintText: AppLocalizations.of(context)!.filterText,
                ),
              ),
              TextField(
                onChanged: (value) {
                  setState(() {
                    selectedAmount = double.tryParse(value);
                    _showClear = true;
                  });
                  widget.onFilterChanged('amount', selectedAmount);
                },
                decoration: InputDecoration(
                  hintText: AppLocalizations.of(context)!.filterAmount,
                ),
              ),
              Row(
                children: [
                  ElevatedButton(
                    onPressed: () async {
                      final DateTime? pickedDate = await showDatePicker(
                        context: context,
                        initialDate: DateTime.now(),
                        firstDate: DateTime(2000),
                        lastDate: DateTime(2101),
                      );
                      if (pickedDate != null &&
                          pickedDate.toString() != selectedDateMin) {
                        setState(() {
                          selectedDateMin = pickedDate.toString();
                          _showClear = true;
                        });
                        widget.onFilterChanged('dateMin', selectedDateMin);
                      }
                    },
                    child: Text(selectedDateMin == null
                        ? AppLocalizations.of(context)!.filterDateMin
                        : selectedDateMin!.split(' ')[0]),
                  ),
                  ElevatedButton(
                    onPressed: () async {
                      final DateTime? pickedDate = await showDatePicker(
                        context: context,
                        initialDate: DateTime.now(),
                        firstDate: DateTime(2000),
                        lastDate: DateTime(2101),
                      );
                      if (pickedDate != null &&
                          pickedDate.toString() != selectedDateMax) {
                        setState(() {
                          selectedDateMax = pickedDate.toString();
                          _showClear = true;
                        });
                        widget.onFilterChanged('dateMax', selectedDateMax);
                      }
                    },
                    child: Text(selectedDateMax == null
                        ? AppLocalizations.of(context)!.filterDateMax
                        : selectedDateMax!.split(' ')[0]),
                  ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }
}
