import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:budget_app/common/ui/bottomsheet_text_form_field.dart';
import 'package:budget_app/common/utils/date_time_formatter.dart';
import 'package:budget_app/features/budgets/controller/budgets_list_controller.dart';
import 'package:budget_app/features/transactions/controller/transactions_list_controller.dart';
import 'package:budget_app/models/Budget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class AddTransactionBottomSheet extends ConsumerStatefulWidget {
  const AddTransactionBottomSheet({
    super.key,
  });

  @override
  AddTransactionBottomSheetState createState() =>
      AddTransactionBottomSheetState();
}

class AddTransactionBottomSheetState
    extends ConsumerState<AddTransactionBottomSheet> {
  final formGlobalKey = GlobalKey<FormState>();

  final transactionNameController = TextEditingController();
  final transactionAmountController = TextEditingController();
  final transactionCurrencyController = TextEditingController();
  final transactionDateController = TextEditingController();
  final transactionBudgetController = TextEditingController();
  final transactionDescriptionController = TextEditingController();

  DateTime? startDate;
  DateTime? endDate;

  @override
  Widget build(BuildContext context) {
    final asyncValue = ref.watch(budgetsListControllerProvider);

    List<Budget> budgetsList = [];
    asyncValue.when(
      data: (budgets) {
        budgetsList = budgets;
      },
      loading: () {},
      error: (error, stackTrace) {},
    );

    final List<String> budgetNames =
        budgetsList.map((budget) => budget.budgetName).toList();

    if (transactionBudgetController.text.isEmpty) {
      if (budgetsList.isNotEmpty) {
        final firstBudget = budgetsList.first;
        transactionBudgetController.text = firstBudget.id;
        startDate = firstBudget.startDate.getDateTime();
        endDate = firstBudget.endDate.getDateTime();
      }
    }

    return Form(
      key: formGlobalKey,
      child: Container(
        padding: EdgeInsets.only(
          top: 15,
          left: 15,
          right: 15,
          bottom: MediaQuery.of(context).viewInsets.bottom + 15,
        ),
        width: double.infinity,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            BottomSheetTextFormField(
              labelText: AppLocalizations.of(context)!.transactionName,
              controller: transactionNameController,
              keyboardType: TextInputType.text,
            ),
            const SizedBox(
              height: 20,
            ),
            BottomSheetTextFormField(
              labelText: AppLocalizations.of(context)!.transactionAmount,
              controller: transactionAmountController,
              keyboardType:
                  const TextInputType.numberWithOptions(decimal: true),
            ),
            const SizedBox(
              height: 20,
            ),
            // BottomSheetTextFormField(
            //   labelText: 'Currency',
            //   controller: transactionCurrencyController,
            //   keyboardType: TextInputType.text,
            // ),
            // const SizedBox(
            //   height: 20,
            // ),
            DropdownButtonFormField<String>(
              value: transactionBudgetController.text.isEmpty
                  ? budgetNames.isNotEmpty
                      ? budgetNames.first
                      : null
                  : budgetsList
                      .firstWhere((budget) =>
                          budget.id == transactionBudgetController.text)
                      .budgetName,
              onChanged: (String? newValue) {
                setState(() {
                  final selectedBudget = budgetsList
                      .firstWhere((budget) => budget.budgetName == newValue);
                  transactionBudgetController.text = selectedBudget.id;
                  startDate = selectedBudget.startDate.getDateTime();
                  endDate = selectedBudget.endDate.getDateTime();
                  safePrint("onchange: ${transactionBudgetController.text}");
                });
              },
              items: budgetNames.isEmpty
                  ? []
                  : budgetNames.map<DropdownMenuItem<String>>((String value) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text(value),
                      );
                    }).toList(),
              decoration: InputDecoration(
                labelText: AppLocalizations.of(context)!.budgetName,
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            BottomSheetTextFormField(
              labelText: AppLocalizations.of(context)!.transactionDate,
              controller: transactionDateController,
              keyboardType: TextInputType.datetime,
              onTap: () async {
                DateTime initialDate = DateTime.now();
                if (startDate != null && endDate != null) {
                  if (initialDate.isBefore(startDate!) ||
                      initialDate.isAfter(endDate!)) {
                    initialDate = startDate!;
                  }
                } else if (startDate != null) {
                  initialDate = startDate!;
                }
                final pickedDate = await showDatePicker(
                  context: context,
                  initialDate: initialDate,
                  firstDate: startDate ?? DateTime(2000),
                  lastDate: endDate ?? DateTime(2101),
                );
                if (pickedDate != null) {
                  transactionDateController.text =
                      pickedDate.format('yyyy-MM-dd');
                }
              },
            ),
            const SizedBox(
              height: 20,
            ),
            TextButton(
              child: Text(AppLocalizations.of(context)!.validateTransaction),
              onPressed: () async {
                final currentState = formGlobalKey.currentState;
                if (currentState == null) {
                  return;
                }
                if (currentState.validate()) {
                  await ref
                      .watch(transactionsListControllerProvider.notifier)
                      .addTransaction(
                        name: transactionNameController.text,
                        amount: double.parse(transactionAmountController.text),
                        currency: "CHF",
                        date: transactionDateController.text,
                        budgetId: transactionBudgetController.text,
                        description: transactionDescriptionController.text,
                      );

                  if (context.mounted) {
                    context.pop();
                  }
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
