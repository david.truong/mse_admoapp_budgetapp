import 'package:amplify_auth_cognito/amplify_auth_cognito.dart';
import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:budget_app/common/ui/bottom_nav_bar.dart';
import 'package:budget_app/common/ui/top_app_bar.dart';
import 'package:budget_app/common/utils/colors.dart' as constants;
import 'package:budget_app/features/transactions/controller/transactions_list_controller.dart';
import 'package:budget_app/features/transactions/ui/transaction_card/transaction_list_card.dart';
import 'package:budget_app/features/transactions/ui/transaction_filter/transaction_filter.dart';
import 'package:budget_app/features/transactions/ui/transactions_list/add_transaction_bottomsheet.dart';
import 'package:budget_app/models/Transaction.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:budget_app/common/navigation/router/routes.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class TransactionsListPage extends ConsumerStatefulWidget {
  const TransactionsListPage({super.key});

  @override
  _TransactionsListPageState createState() => _TransactionsListPageState();
}

class _TransactionsListPageState extends ConsumerState<TransactionsListPage> {
  String? selectedText;
  double? selectedAmount;
  String? selectedDateMin;
  String? selectedDateMax;

  void applyFilter(String key, dynamic value) {
    setState(() {
      if (key == 'text') {
        selectedText = value;
      } else if (key == 'amount') {
        selectedAmount = value;
      } else if (key == 'dateMin') {
        selectedDateMin = value;
      } else if (key == 'dateMax') {
        selectedDateMax = value;
      }
    });
  }

  Future<void> showAddBudgetDialog(BuildContext context) =>
      showModalBottomSheet<void>(
        isScrollControlled: true,
        elevation: 5,
        context: context,
        builder: (sheetContext) {
          return const AddTransactionBottomSheet();
        },
      );

  @override
  Widget build(BuildContext context) {
    final transactionListValue = ref.watch(transactionsListControllerProvider);
    final filteredTransactions = transactionListValue.when(
      data: (transactions) {
        return transactions.where((transaction) {
          if (selectedText != null &&
              !transaction.name
                  .toLowerCase()
                  .contains(selectedText!.toLowerCase())) {
            return false;
          }
          if (selectedAmount != null && transaction.amount != selectedAmount) {
            return false;
          }
          if (selectedDateMin != null &&
              transaction.date
                  .getDateTime()
                  .isBefore(DateTime.parse(selectedDateMin!))) {
            return false;
          }
          if (selectedDateMax != null &&
              transaction.date
                  .getDateTime()
                  .isAfter(DateTime.parse(selectedDateMax!))) {
            return false;
          }
          return true;
        }).toList();
      },
      loading: () => <Transaction>[],
      error: (error, stackTrace) => <Transaction>[],
    );
    return Scaffold(
        appBar: const TopAppBar(),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            showAddBudgetDialog(context);
          },
          backgroundColor: const Color(constants.primaryColorDark),
          child: const Icon(Icons.add),
        ),
        body: Column(children: [
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Text(
              AppLocalizations.of(context)!.myTransactions,
              style: const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
          ),
          const Divider(
            height: 20,
            thickness: 5,
            indent: 20,
            endIndent: 20,
          ),
          const SizedBox(height: 16),
          TransactionFilter(onFilterChanged: applyFilter),
          Expanded(
            child: switch (transactionListValue) {
              AsyncError() => Center(
                  child: Text(AppLocalizations.of(context)!.error),
                ),
              AsyncLoading() => const Center(
                  child: CircularProgressIndicator(),
                ),
              AsyncValue<List<Transaction>>() =>
                TransactionListCard(transactionList: filteredTransactions),
            },
          ),
        ]),
        bottomNavigationBar:
            BottomNavBar(currentPage: AppRoute.transactionList.name));
  }
}
