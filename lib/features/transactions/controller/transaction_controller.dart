import 'dart:async';
import 'dart:io';

import 'package:budget_app/features/transactions/data/transactions_repository.dart';
import 'package:budget_app/models/ModelProvider.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'transaction_controller.g.dart';

@riverpod
class TransactionController extends _$TransactionController {
  Future<Transaction> _fetchTransaction(String transactionId) async {
    final transactionsRepository = ref.read(transactionsRepositoryProvider);
    return transactionsRepository.getTransaction(transactionId);
  }

  @override
  FutureOr<Transaction> build(String transactionId) async {
    return _fetchTransaction(transactionId);
  }

  Future<void> updateTransaction(Transaction transaction) async {
    state = const AsyncValue.loading();
    state = await AsyncValue.guard(() async {
      final transactionsRepository = ref.read(transactionsRepositoryProvider);
      await transactionsRepository.update(transaction);
      return _fetchTransaction(transaction.id);
    });
  }
}