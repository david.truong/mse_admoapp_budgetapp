// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'transaction_controller.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$transactionControllerHash() =>
    r'4428d7abae3834760438edc796bfd99f2852adcb';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

abstract class _$TransactionController
    extends BuildlessAutoDisposeAsyncNotifier<Transaction> {
  late final String transactionId;

  FutureOr<Transaction> build(
    String transactionId,
  );
}

/// See also [TransactionController].
@ProviderFor(TransactionController)
const transactionControllerProvider = TransactionControllerFamily();

/// See also [TransactionController].
class TransactionControllerFamily extends Family<AsyncValue<Transaction>> {
  /// See also [TransactionController].
  const TransactionControllerFamily();

  /// See also [TransactionController].
  TransactionControllerProvider call(
    String transactionId,
  ) {
    return TransactionControllerProvider(
      transactionId,
    );
  }

  @override
  TransactionControllerProvider getProviderOverride(
    covariant TransactionControllerProvider provider,
  ) {
    return call(
      provider.transactionId,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'transactionControllerProvider';
}

/// See also [TransactionController].
class TransactionControllerProvider
    extends AutoDisposeAsyncNotifierProviderImpl<TransactionController,
        Transaction> {
  /// See also [TransactionController].
  TransactionControllerProvider(
    String transactionId,
  ) : this._internal(
          () => TransactionController()..transactionId = transactionId,
          from: transactionControllerProvider,
          name: r'transactionControllerProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$transactionControllerHash,
          dependencies: TransactionControllerFamily._dependencies,
          allTransitiveDependencies:
              TransactionControllerFamily._allTransitiveDependencies,
          transactionId: transactionId,
        );

  TransactionControllerProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.transactionId,
  }) : super.internal();

  final String transactionId;

  @override
  FutureOr<Transaction> runNotifierBuild(
    covariant TransactionController notifier,
  ) {
    return notifier.build(
      transactionId,
    );
  }

  @override
  Override overrideWith(TransactionController Function() create) {
    return ProviderOverride(
      origin: this,
      override: TransactionControllerProvider._internal(
        () => create()..transactionId = transactionId,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        transactionId: transactionId,
      ),
    );
  }

  @override
  AutoDisposeAsyncNotifierProviderElement<TransactionController, Transaction>
      createElement() {
    return _TransactionControllerProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is TransactionControllerProvider &&
        other.transactionId == transactionId;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, transactionId.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin TransactionControllerRef
    on AutoDisposeAsyncNotifierProviderRef<Transaction> {
  /// The parameter `transactionId` of this provider.
  String get transactionId;
}

class _TransactionControllerProviderElement
    extends AutoDisposeAsyncNotifierProviderElement<TransactionController,
        Transaction> with TransactionControllerRef {
  _TransactionControllerProviderElement(super.provider);

  @override
  String get transactionId =>
      (origin as TransactionControllerProvider).transactionId;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
