// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'transactions_list_controller.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$transactionsListControllerHash() =>
    r'369b4bd9d968d8bdc1ebdff728dcabd2c1027967';

/// See also [TransactionsListController].
@ProviderFor(TransactionsListController)
final transactionsListControllerProvider = AutoDisposeAsyncNotifierProvider<
    TransactionsListController, List<Transaction>>.internal(
  TransactionsListController.new,
  name: r'transactionsListControllerProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$transactionsListControllerHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$TransactionsListController
    = AutoDisposeAsyncNotifier<List<Transaction>>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
