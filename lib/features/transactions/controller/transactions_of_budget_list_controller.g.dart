// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'transactions_of_budget_list_controller.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$transactionsOfBudgetsListControllerHash() =>
    r'2265a27ddc8fb7a3801a75292bfcde36dec068d4';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

abstract class _$TransactionsOfBudgetsListController
    extends BuildlessAutoDisposeAsyncNotifier<List<Transaction>> {
  late final String budgetId;

  FutureOr<List<Transaction>> build(
    String budgetId,
  );
}

/// See also [TransactionsOfBudgetsListController].
@ProviderFor(TransactionsOfBudgetsListController)
const transactionsOfBudgetsListControllerProvider =
    TransactionsOfBudgetsListControllerFamily();

/// See also [TransactionsOfBudgetsListController].
class TransactionsOfBudgetsListControllerFamily
    extends Family<AsyncValue<List<Transaction>>> {
  /// See also [TransactionsOfBudgetsListController].
  const TransactionsOfBudgetsListControllerFamily();

  /// See also [TransactionsOfBudgetsListController].
  TransactionsOfBudgetsListControllerProvider call(
    String budgetId,
  ) {
    return TransactionsOfBudgetsListControllerProvider(
      budgetId,
    );
  }

  @override
  TransactionsOfBudgetsListControllerProvider getProviderOverride(
    covariant TransactionsOfBudgetsListControllerProvider provider,
  ) {
    return call(
      provider.budgetId,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'transactionsOfBudgetsListControllerProvider';
}

/// See also [TransactionsOfBudgetsListController].
class TransactionsOfBudgetsListControllerProvider
    extends AutoDisposeAsyncNotifierProviderImpl<
        TransactionsOfBudgetsListController, List<Transaction>> {
  /// See also [TransactionsOfBudgetsListController].
  TransactionsOfBudgetsListControllerProvider(
    String budgetId,
  ) : this._internal(
          () => TransactionsOfBudgetsListController()..budgetId = budgetId,
          from: transactionsOfBudgetsListControllerProvider,
          name: r'transactionsOfBudgetsListControllerProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$transactionsOfBudgetsListControllerHash,
          dependencies: TransactionsOfBudgetsListControllerFamily._dependencies,
          allTransitiveDependencies: TransactionsOfBudgetsListControllerFamily
              ._allTransitiveDependencies,
          budgetId: budgetId,
        );

  TransactionsOfBudgetsListControllerProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.budgetId,
  }) : super.internal();

  final String budgetId;

  @override
  FutureOr<List<Transaction>> runNotifierBuild(
    covariant TransactionsOfBudgetsListController notifier,
  ) {
    return notifier.build(
      budgetId,
    );
  }

  @override
  Override overrideWith(TransactionsOfBudgetsListController Function() create) {
    return ProviderOverride(
      origin: this,
      override: TransactionsOfBudgetsListControllerProvider._internal(
        () => create()..budgetId = budgetId,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        budgetId: budgetId,
      ),
    );
  }

  @override
  AutoDisposeAsyncNotifierProviderElement<TransactionsOfBudgetsListController,
      List<Transaction>> createElement() {
    return _TransactionsOfBudgetsListControllerProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is TransactionsOfBudgetsListControllerProvider &&
        other.budgetId == budgetId;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, budgetId.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin TransactionsOfBudgetsListControllerRef
    on AutoDisposeAsyncNotifierProviderRef<List<Transaction>> {
  /// The parameter `budgetId` of this provider.
  String get budgetId;
}

class _TransactionsOfBudgetsListControllerProviderElement
    extends AutoDisposeAsyncNotifierProviderElement<
        TransactionsOfBudgetsListController,
        List<Transaction>> with TransactionsOfBudgetsListControllerRef {
  _TransactionsOfBudgetsListControllerProviderElement(super.provider);

  @override
  String get budgetId =>
      (origin as TransactionsOfBudgetsListControllerProvider).budgetId;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
