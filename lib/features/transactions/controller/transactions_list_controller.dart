import 'dart:async';
import 'dart:ffi';

import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:budget_app/features/transactions/data/transactions_repository.dart';
import 'package:budget_app/models/ModelProvider.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'transactions_list_controller.g.dart';

@riverpod
class TransactionsListController extends _$TransactionsListController {
  Future<List<Transaction>> _fetchTransactions() async {
    final transactionsRepository = ref.read(transactionsRepositoryProvider);
    final transactions = await transactionsRepository.getAllTransactions();
    return transactions;
  }

  @override
  FutureOr<List<Transaction>> build() async {
    return _fetchTransactions();
  }

  Future<void> addTransaction({
    required String name,
    required double amount,
    required String currency,
    required String date,
    required String budgetId,
    String? description,
  }) async {
    final transaction = Transaction(
        name: name,
        amount: amount,
        currency: currency,
        date: TemporalDate(DateTime.parse(date)),
        description: description,
        budgetID: budgetId);
    safePrint(transaction);
    state = const AsyncValue.loading();

    state = await AsyncValue.guard(() async {
      final transactionsRepository = ref.read(transactionsRepositoryProvider);
      await transactionsRepository.add(transaction);
      return _fetchTransactions();
    });
  }

  Future<void> removeTransaction(Transaction transaction) async {
    state = const AsyncValue.loading();
    state = await AsyncValue.guard(() async {
      final transactionsRepository = ref.read(transactionsRepositoryProvider);
      await transactionsRepository.delete(transaction);

      return _fetchTransactions();
    });
  }
}
