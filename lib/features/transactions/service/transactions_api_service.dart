import 'dart:async';

import 'package:amplify_api/amplify_api.dart';
import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:budget_app/models/ModelProvider.dart';
import 'package:budget_app/models/Transaction.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final transactionsAPIServiceProvider = Provider<TransactionsAPIService>((ref) {
  final service = TransactionsAPIService();
  return service;
});

class TransactionsAPIService {
  TransactionsAPIService();

  Future<List<Transaction>> getTransactionsWithBudgetID(String budgetID) async {
    try {
      final request = ModelQueries.list(Transaction.classType);
      final response = await Amplify.API.query(request: request).response;

      final transactions = response.data?.items;
      if (transactions == null) {
        safePrint('getTransactionsWithBudgetID errors: ${response.errors}');
        return const [];
      }
      transactions.sort(
        (a, b) =>
            a!.date.getDateTime().compareTo(b!.date.getDateTime()),
      );
      return transactions
          .map((e) => e as Transaction)
          .where(
            (element) => element.budgetID == budgetID
          )
          .toList();
    } on Exception catch (error) {
      safePrint('getTransactionsWithBudgetID failed: $error');

      return const [];
    }
  }

  Future<List<Transaction>> getAllTransactions() async {
    try {
      final request = ModelQueries.list(Transaction.classType);
      final response = await Amplify.API.query(request: request).response;

      final transactions = response.data?.items;
      if (transactions == null) {
        safePrint('getAllTransactions errors: ${response.errors}');
        return const [];
      }
      transactions.sort(
        (a, b) =>
            a!.date.getDateTime().compareTo(b!.date.getDateTime()),
      );
      return transactions.map((e) => e as Transaction).toList();
    } on Exception catch (error) {
      safePrint('getAllTransactions failed: $error');

      return const [];
    }
  }

  Future<void> addTransaction(Transaction transaction) async {
    try {
      final request = ModelMutations.create(transaction);
      final response = await Amplify.API.mutate(request: request).response;

      final createdTransaction = response.data;
      if (createdTransaction == null) {
        safePrint('addTransaction errors: ${response.errors}');
        return;
      }
    } on Exception catch (error) {
      safePrint('addTransaction failed: $error');
    }
  }

  Future<void> deleteTransaction(Transaction transaction) async {
    try {
      await Amplify.API
          .mutate(
            request: ModelMutations.delete(transaction),
          )
          .response;
    } on Exception catch (error) {
      safePrint('deleteTransaction failed: $error');
    }
  }

  Future<void> updateTransaction(Transaction updatedTransaction) async {
    try {
      await Amplify.API
          .mutate(
            request: ModelMutations.update(updatedTransaction),
          )
          .response;
    } on Exception catch (error) {
      safePrint('updateTransaction failed: $error');
    }
  }

  Future<Transaction> getTransaction(String transactionId) async {
    try {
      final request = ModelQueries.get(
        Transaction.classType,
        TransactionModelIdentifier(id: transactionId),
      );
      final response = await Amplify.API.query(request: request).response;

      final transaction = response.data!;
      return transaction;
    } on Exception catch (error) {
      safePrint('getTransaction failed: $error');
      rethrow;
    }
  }
}