import 'package:budget_app/features/transactions/service/transactions_api_service.dart';
import 'package:budget_app/models/Transaction.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final transactionsRepositoryProvider = Provider<TransactionsRepository>((ref) {
  final transactionsAPIService = ref.read(transactionsAPIServiceProvider);
  return TransactionsRepository(transactionsAPIService);
});

class TransactionsRepository {
  TransactionsRepository(this.transactionsAPIService);

  final TransactionsAPIService transactionsAPIService;

  Future<List<Transaction>> getTransactionsWithBudgetID(String budgetId) {
    return transactionsAPIService.getTransactionsWithBudgetID(budgetId);
  }

  Future<List<Transaction>> getAllTransactions() {
    return transactionsAPIService.getAllTransactions();
  }

  Future<void> add(Transaction transaction) async {
    return transactionsAPIService.addTransaction(transaction);
  }

  Future<void> update(Transaction updatedTransaction) async {
    return transactionsAPIService.updateTransaction(updatedTransaction);
  }

  Future<void> delete(Transaction deletedTransaction) async {
    return transactionsAPIService.deleteTransaction(deletedTransaction);
  }

  Future<Transaction> getTransaction(String transactionId) async {
    return transactionsAPIService.getTransaction(transactionId);
  }
}
