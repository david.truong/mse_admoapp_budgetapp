import 'package:amplify_auth_cognito/amplify_auth_cognito.dart';
import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:budget_app/common/ui/bottom_nav_bar.dart';
import 'package:budget_app/common/ui/top_app_bar.dart';
import 'package:budget_app/common/utils/colors.dart' as constants;
import 'package:budget_app/features/budgets/controller/budgets_list_controller.dart';
import 'package:budget_app/features/transactions/controller/transactions_list_controller.dart';
import 'package:budget_app/models/Budget.dart';
import 'package:budget_app/models/Transaction.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:budget_app/common/navigation/router/routes.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:fl_chart/fl_chart.dart';

class HomePage extends ConsumerWidget {
  const HomePage({
    super.key,
  });

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final transactionListValue = ref.watch(transactionsListControllerProvider);
    final budgetsListValue = ref.watch(budgetsListControllerProvider);

    Map<String, double> budgetAmounts = {};
    double totalAmount = 0;

    if (transactionListValue is AsyncData<List<Transaction>>) {
      final transactions = transactionListValue.value;
      for (var transaction in transactions) {
        if (budgetAmounts.containsKey(transaction.budgetID)) {
          budgetAmounts[transaction.budgetID] =
              budgetAmounts[transaction.budgetID]! + transaction.amount;
        } else {
          budgetAmounts[transaction.budgetID] = transaction.amount;
        }
      }
      totalAmount = budgetAmounts.values.fold(0, (sum, amount) => sum + amount);
    }

    List<Map<String, dynamic>> sectionData = [];
    List<Widget> legends = [];
    if (budgetsListValue is AsyncData<List<Budget>>) {
      final budgets = budgetsListValue.value;
      for (var budget in budgets) {
        if (budgetAmounts.containsKey(budget.id)) {
          final amount = budgetAmounts[budget.id]!;
          final color = _getColorForBudget(budget.id);
          sectionData.add({
            'budget': budget,
            'section': PieChartSectionData(
              color: color,
              value: amount / totalAmount * 100,
              title: '${(amount / totalAmount * 100).toStringAsFixed(0)}%',
            ),
          });
          legends.add(
            Row(
              children: [
                Container(
                  width: 16,
                  height: 16,
                  color: color,
                ),
                const SizedBox(width: 8),
                Text('${budget.budgetName} (${amount.toStringAsFixed(2)})'),
              ],
            ),
          );
        }
      }
    }

    return Scaffold(
        appBar: const TopAppBar(),
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text(
                AppLocalizations.of(context)!.homePageTitle,
                style:
                    const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
              ),
            ),
            const Divider(
              height: 20,
              thickness: 5,
              indent: 20,
              endIndent: 20,
            ),
            const SizedBox(height: 16),
            if (transactionListValue is AsyncLoading ||
                budgetsListValue is AsyncLoading)
              const Center(child: CircularProgressIndicator())
            else if (transactionListValue is AsyncError ||
                budgetsListValue is AsyncError)
              Center(child: Text(AppLocalizations.of(context)!.error))
            else if (transactionListValue is AsyncData<List<Transaction>> &&
                budgetsListValue is AsyncData<List<Budget>>)
              Expanded(
                child: Column(
                  children: [
                    SizedBox(
                      height: 200,
                      child: PieChart(
                        PieChartData(
                          sections: sectionData
                              .map((data) =>
                                  data['section'] as PieChartSectionData)
                              .toList(),
                          pieTouchData: PieTouchData(
                              touchCallback: (event, pieTouchResponse) {
                            if (pieTouchResponse != null &&
                                pieTouchResponse.touchedSection != null) {
                              final touchedIndex = pieTouchResponse
                                  .touchedSection!.touchedSectionIndex;
                              final budget =
                                  sectionData[touchedIndex]['budget'] as Budget;
                              context.goNamed(AppRoute.budget.name,
                                  pathParameters: {'id': budget.id});
                            }
                          }),
                        ),
                      ),
                    ),
                    const SizedBox(height: 16),
                    Text(
                      '${AppLocalizations.of(context)!.totalAmount}: ${totalAmount.toStringAsFixed(2)}',
                      style: const TextStyle(
                          fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                    const SizedBox(height: 24),
                    Row(children: [
                      const SizedBox(width: 16),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: legends,
                      ),
                    ]),
                  ],
                ),
              )
          ],
        ),
        bottomNavigationBar: BottomNavBar(currentPage: AppRoute.home.name));
  }

  Color _getColorForBudget(String budgetId) {
    final colors = [
      Colors.red,
      Colors.green,
      Colors.blue,
      Colors.yellow,
      Colors.purple,
    ];
    return colors[budgetId.hashCode % colors.length];
  }
}
