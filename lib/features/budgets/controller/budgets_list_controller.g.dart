// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'budgets_list_controller.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$budgetsListControllerHash() =>
    r'590f3f30ea61d8f35fddf4b6a0d8d38e0c7018da';

/// See also [BudgetsListController].
@ProviderFor(BudgetsListController)
final budgetsListControllerProvider = AutoDisposeAsyncNotifierProvider<
    BudgetsListController, List<Budget>>.internal(
  BudgetsListController.new,
  name: r'budgetsListControllerProvider',
  debugGetCreateSourceHash: const bool.fromEnvironment('dart.vm.product')
      ? null
      : _$budgetsListControllerHash,
  dependencies: null,
  allTransitiveDependencies: null,
);

typedef _$BudgetsListController = AutoDisposeAsyncNotifier<List<Budget>>;
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
