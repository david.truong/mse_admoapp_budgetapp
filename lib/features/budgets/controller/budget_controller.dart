import 'dart:async';
import 'dart:io';

import 'package:budget_app/common/services/storage_service.dart';
import 'package:budget_app/features/budgets/data/budgets_repository.dart';
import 'package:budget_app/models/ModelProvider.dart';
import 'package:flutter/material.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'budget_controller.g.dart';

@riverpod
class BudgetController extends _$BudgetController {
  Future<Budget> _fetchBudget(String budgetId) async {
    final budgetsRepository = ref.read(budgetsRepositoryProvider);
    return budgetsRepository.getBudget(budgetId);
  }

  @override
  FutureOr<Budget> build(String budgetId) async {
    return _fetchBudget(budgetId);
  }

  Future<void> updateBudget(Budget budget) async {
    state = const AsyncValue.loading();
    state = await AsyncValue.guard(() async {
      final budgetsRepository = ref.read(budgetsRepositoryProvider);
      await budgetsRepository.update(budget);
      return _fetchBudget(budget.id);
    });
  }

  Future<void> uploadFile(File file, Budget budget) async {
    final fileKey = await ref.read(storageServiceProvider).uploadFile(file);
    if (fileKey != null) {
      final imageUrl =
        await ref.read(storageServiceProvider).getImageUrl(fileKey);
      final updatedBudget =
        budget.copyWith(budgetImageKey: fileKey, budgetImageUrl: imageUrl);
      await ref.read(budgetsRepositoryProvider).update(updatedBudget);
      ref.read(storageServiceProvider).resetUploadProgress();
    }
  }

  ValueNotifier<double> uploadProgress() {
    return ref.read(storageServiceProvider).getUploadProgress();
  }
}