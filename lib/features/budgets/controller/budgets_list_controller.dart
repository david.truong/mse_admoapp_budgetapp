import 'dart:async';

import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:budget_app/features/budgets/data/budgets_repository.dart';
import 'package:budget_app/models/ModelProvider.dart';
import 'package:riverpod_annotation/riverpod_annotation.dart';

part 'budgets_list_controller.g.dart';

@riverpod
class BudgetsListController extends _$BudgetsListController {
  Future<List<Budget>> _fetchBudgets() async {
    final budgetsRepository = ref.read(budgetsRepositoryProvider);
    final budgets = await budgetsRepository.getBudgets();
    return budgets;
  }

  @override
  FutureOr<List<Budget>> build() async {
    return _fetchBudgets();
  }

  Future<void> addBudget({
    required String name,
    required String destination,
    required int limit,
    required String startDate,
    required String endDate,
  }) async {
    final budget = Budget(
      budgetName: name,
      destination: destination,
      limit: limit,
      startDate: TemporalDate(DateTime.parse(startDate)),
      endDate: TemporalDate(DateTime.parse(endDate)),
    );

    state = const AsyncValue.loading();

    state = await AsyncValue.guard(() async {
      final budgetsRepository = ref.read(budgetsRepositoryProvider);
      await budgetsRepository.add(budget);
      return _fetchBudgets();
    });
  }

  Future<void> removeBudget(Budget budget) async {
    state = const AsyncValue.loading();
    state = await AsyncValue.guard(() async {
      final budgetsRepository = ref.read(budgetsRepositoryProvider);
      await budgetsRepository.delete(budget);

      return _fetchBudgets();
    });
  }
}