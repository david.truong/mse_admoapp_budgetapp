// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'budget_controller.dart';

// **************************************************************************
// RiverpodGenerator
// **************************************************************************

String _$budgetControllerHash() => r'8afb543bb5ca89b319e141a741770769af5b6d39';

/// Copied from Dart SDK
class _SystemHash {
  _SystemHash._();

  static int combine(int hash, int value) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + value);
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x0007ffff & hash) << 10));
    return hash ^ (hash >> 6);
  }

  static int finish(int hash) {
    // ignore: parameter_assignments
    hash = 0x1fffffff & (hash + ((0x03ffffff & hash) << 3));
    // ignore: parameter_assignments
    hash = hash ^ (hash >> 11);
    return 0x1fffffff & (hash + ((0x00003fff & hash) << 15));
  }
}

abstract class _$BudgetController
    extends BuildlessAutoDisposeAsyncNotifier<Budget> {
  late final String budgetId;

  FutureOr<Budget> build(
    String budgetId,
  );
}

/// See also [BudgetController].
@ProviderFor(BudgetController)
const budgetControllerProvider = BudgetControllerFamily();

/// See also [BudgetController].
class BudgetControllerFamily extends Family<AsyncValue<Budget>> {
  /// See also [BudgetController].
  const BudgetControllerFamily();

  /// See also [BudgetController].
  BudgetControllerProvider call(
    String budgetId,
  ) {
    return BudgetControllerProvider(
      budgetId,
    );
  }

  @override
  BudgetControllerProvider getProviderOverride(
    covariant BudgetControllerProvider provider,
  ) {
    return call(
      provider.budgetId,
    );
  }

  static const Iterable<ProviderOrFamily>? _dependencies = null;

  @override
  Iterable<ProviderOrFamily>? get dependencies => _dependencies;

  static const Iterable<ProviderOrFamily>? _allTransitiveDependencies = null;

  @override
  Iterable<ProviderOrFamily>? get allTransitiveDependencies =>
      _allTransitiveDependencies;

  @override
  String? get name => r'budgetControllerProvider';
}

/// See also [BudgetController].
class BudgetControllerProvider
    extends AutoDisposeAsyncNotifierProviderImpl<BudgetController, Budget> {
  /// See also [BudgetController].
  BudgetControllerProvider(
    String budgetId,
  ) : this._internal(
          () => BudgetController()..budgetId = budgetId,
          from: budgetControllerProvider,
          name: r'budgetControllerProvider',
          debugGetCreateSourceHash:
              const bool.fromEnvironment('dart.vm.product')
                  ? null
                  : _$budgetControllerHash,
          dependencies: BudgetControllerFamily._dependencies,
          allTransitiveDependencies:
              BudgetControllerFamily._allTransitiveDependencies,
          budgetId: budgetId,
        );

  BudgetControllerProvider._internal(
    super._createNotifier, {
    required super.name,
    required super.dependencies,
    required super.allTransitiveDependencies,
    required super.debugGetCreateSourceHash,
    required super.from,
    required this.budgetId,
  }) : super.internal();

  final String budgetId;

  @override
  FutureOr<Budget> runNotifierBuild(
    covariant BudgetController notifier,
  ) {
    return notifier.build(
      budgetId,
    );
  }

  @override
  Override overrideWith(BudgetController Function() create) {
    return ProviderOverride(
      origin: this,
      override: BudgetControllerProvider._internal(
        () => create()..budgetId = budgetId,
        from: from,
        name: null,
        dependencies: null,
        allTransitiveDependencies: null,
        debugGetCreateSourceHash: null,
        budgetId: budgetId,
      ),
    );
  }

  @override
  AutoDisposeAsyncNotifierProviderElement<BudgetController, Budget>
      createElement() {
    return _BudgetControllerProviderElement(this);
  }

  @override
  bool operator ==(Object other) {
    return other is BudgetControllerProvider && other.budgetId == budgetId;
  }

  @override
  int get hashCode {
    var hash = _SystemHash.combine(0, runtimeType.hashCode);
    hash = _SystemHash.combine(hash, budgetId.hashCode);

    return _SystemHash.finish(hash);
  }
}

mixin BudgetControllerRef on AutoDisposeAsyncNotifierProviderRef<Budget> {
  /// The parameter `budgetId` of this provider.
  String get budgetId;
}

class _BudgetControllerProviderElement
    extends AutoDisposeAsyncNotifierProviderElement<BudgetController, Budget>
    with BudgetControllerRef {
  _BudgetControllerProviderElement(super.provider);

  @override
  String get budgetId => (origin as BudgetControllerProvider).budgetId;
}
// ignore_for_file: type=lint
// ignore_for_file: subtype_of_sealed_class, invalid_use_of_internal_member, invalid_use_of_visible_for_testing_member
