import 'package:budget_app/features/budgets/service/budgets_api_service.dart';
import 'package:budget_app/models/Budget.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final budgetsRepositoryProvider = Provider<BudgetsRepository>((ref) {
  final budgetsAPIService = ref.read(budgetsAPIServiceProvider);
  return BudgetsRepository(budgetsAPIService);
});

class BudgetsRepository {
  BudgetsRepository(this.budgetsAPIService);

  final BudgetsAPIService budgetsAPIService;

  Future<List<Budget>> getBudgets() {
    return budgetsAPIService.getBudgets();
  }

  Future<List<Budget>> getPastBudgets() {
    return budgetsAPIService.getPastBudgets();
  }

  Future<void> add(Budget budget) async {
    return budgetsAPIService.addBudget(budget);
  }

  Future<void> update(Budget updatedBudget) async {
    return budgetsAPIService.updateBudget(updatedBudget);
  }

  Future<void> delete(Budget deletedBudget) async {
    return budgetsAPIService.deleteBudget(deletedBudget);
  }

  Future<Budget> getBudget(String budgetId) async {
    return budgetsAPIService.getBudget(budgetId);
  }
}