import 'dart:async';

import 'package:amplify_api/amplify_api.dart';
import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:budget_app/models/ModelProvider.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

final budgetsAPIServiceProvider = Provider<BudgetsAPIService>((ref) {
  final service = BudgetsAPIService();
  return service;
});

class BudgetsAPIService {
  BudgetsAPIService();

  Future<List<Budget>> getBudgets() async {
    try {
      final request = ModelQueries.list(Budget.classType);
      final response = await Amplify.API.query(request: request).response;

      final budgets = response.data?.items;
      if (budgets == null) {
        safePrint('getBudgets errors: ${response.errors}');
        return const [];
      }
      budgets.sort(
        (a, b) =>
            a!.startDate.getDateTime().compareTo(b!.startDate.getDateTime()),
      );
      return budgets
          .map((e) => e as Budget)
          .where(
            (element) => element.endDate.getDateTime().isAfter(DateTime.now()),
          )
          .toList();
    } on Exception catch (error) {
      safePrint('getBudgets failed: $error');

      return const [];
    }
  }

  Future<List<Budget>> getPastBudgets() async {
    try {
      final request = ModelQueries.list(Budget.classType);
      final response = await Amplify.API.query(request: request).response;

      final budgets = response.data?.items;
      if (budgets == null) {
        safePrint('getPastBudgets errors: ${response.errors}');
        return const [];
      }
      budgets.sort(
        (a, b) =>
            a!.startDate.getDateTime().compareTo(b!.startDate.getDateTime()),
      );
      return budgets
          .map((e) => e as Budget)
          .where(
            (element) => element.endDate.getDateTime().isBefore(DateTime.now()),
          )
          .toList();
    } on Exception catch (error) {
      safePrint('getPastBudgets failed: $error');

      return const [];
    }
  }

  Future<void> addBudget(Budget budget) async {
    try {
      final request = ModelMutations.create(budget);
      final response = await Amplify.API.mutate(request: request).response;

      final createdBudget = response.data;
      if (createdBudget == null) {
        safePrint('addBudget errors: ${response.errors}');
        return;
      }
    } on Exception catch (error) {
      safePrint('addBudget failed: $error');
    }
  }

  Future<void> deleteBudget(Budget budget) async {
    try {
      await Amplify.API
          .mutate(
            request: ModelMutations.delete(budget),
          )
          .response;
    } on Exception catch (error) {
      safePrint('deleteBudget failed: $error');
    }
  }

  Future<void> updateBudget(Budget updatedBudget) async {
    try {
      await Amplify.API
          .mutate(
            request: ModelMutations.update(updatedBudget),
          )
          .response;
    } on Exception catch (error) {
      safePrint('updateBudget failed: $error');
    }
  }

  Future<Budget> getBudget(String budgetId) async {
    try {
      final request = ModelQueries.get(
        Budget.classType,
        BudgetModelIdentifier(id: budgetId),
      );
      final response = await Amplify.API.query(request: request).response;

      final budget = response.data!;
      return budget;
    } on Exception catch (error) {
      safePrint('getBudget failed: $error');
      rethrow;
    }
  }
}