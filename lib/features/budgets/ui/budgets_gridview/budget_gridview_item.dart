import 'package:budget_app/common/navigation/router/routes.dart';
import 'package:budget_app/features/budgets/ui/budgets_gridview/budget_gridview_item_card.dart';
import 'package:budget_app/models/ModelProvider.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class BudgetGridViewItem extends StatelessWidget {
  const BudgetGridViewItem({
    required this.budget,
    super.key,
  });

  final Budget budget;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      splashColor: Theme.of(context).primaryColor,
      borderRadius: BorderRadius.circular(15),
      onTap: () {
        context.goNamed(
          AppRoute.budget.name,
          pathParameters: {'id': budget.id},
          extra: budget,
        );
      },
      child: BudgetGridViewItemCard(
        budget: budget,
      ),
    );
  }
}