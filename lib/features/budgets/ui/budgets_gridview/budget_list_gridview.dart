import 'package:budget_app/features/budgets/ui/budgets_gridview/budget_gridview_item.dart';
import 'package:budget_app/models/ModelProvider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class BudgetsListGridView extends StatelessWidget {
  const BudgetsListGridView({
    required this.budgetsList,
    super.key,
  });

  final AsyncValue<List<Budget>> budgetsList;

  @override
  Widget build(BuildContext context) {
    switch (budgetsList) {
      case AsyncData(:final value):
        return value.isEmpty
            ? Center(
                child: Text(AppLocalizations.of(context)!.noBudget),
              )
            : OrientationBuilder(
                builder: (context, orientation) {
                  return GridView.count(
                    crossAxisCount:
                        (orientation == Orientation.portrait) ? 2 : 3,
                    mainAxisSpacing: 4,
                    crossAxisSpacing: 4,
                    padding: const EdgeInsets.all(4),
                    childAspectRatio:
                        (orientation == Orientation.portrait) ? 0.9 : 1.4,
                    children: value.map((budgetData) {
                      return BudgetGridViewItem(
                        budget: budgetData,
                      );
                    }).toList(growable: false),
                  );
                },
              );

      case AsyncError():
        return Center(
          child: Text(AppLocalizations.of(context)!.error),
        );
      case AsyncLoading():
        return const Center(
          child: CircularProgressIndicator(),
        );

      case _:
        return Center(
          child: Text(AppLocalizations.of(context)!.error),
        );
    }
  }
}
