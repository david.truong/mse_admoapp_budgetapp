import 'dart:math';
import 'package:budget_app/common/utils/colors.dart' as constants;
import 'package:budget_app/common/utils/date_time_formatter.dart';
import 'package:budget_app/features/transactions/controller/transactions_list_controller.dart';
import 'package:budget_app/models/ModelProvider.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

class BudgetGridViewItemCard extends ConsumerWidget {
  const BudgetGridViewItemCard({
    super.key,
    required this.budget,
  });

  final Budget budget;

  Color getRandomColor() {
    final Random random = Random();
    return Color.fromARGB(
      255,
      random.nextInt(256),
      random.nextInt(256),
      random.nextInt(256),
    );
  }

  double calculateUsedPercentage(List<Transaction> transactions) {
    final double totalSpent = transactions
        .where((transaction) => transaction.budgetID == budget.id)
        .fold(0.0, (sum, transaction) => sum + transaction.amount);
    return (totalSpent / (budget.limit ?? 0)) * 100;
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final transactionListValue = ref.watch(transactionsListControllerProvider);
    final transactions = transactionListValue.when(
      data: (transactions) {
        return transactions.toList();
      },
      loading: () => <Transaction>[],
      error: (error, stackTrace) => <Transaction>[],
    );
    final double usedPercentage = calculateUsedPercentage(transactions);

    return Card(
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
      elevation: 5,
      child: Column(
        children: [
          Expanded(
            child: Container(
              height: 500,
              alignment: Alignment.center,
              color: const Color(constants.primaryColorDark),
              child: Stack(
                children: [
                  Positioned.fill(
                    child: budget.budgetImageUrl != null
                        ? Stack(
                            children: [
                              const Center(child: CircularProgressIndicator()),
                              CachedNetworkImage(
                                errorWidget: (context, url, dynamic error) =>
                                    const Icon(Icons.error_outline_outlined),
                                imageUrl: budget.budgetImageUrl!,
                                cacheKey: budget.budgetImageKey,
                                width: double.maxFinite,
                                height: 500,
                                alignment: Alignment.topCenter,
                                fit: BoxFit.fill,
                              ),
                            ],
                          )
                        : Center(
                            child: ColoredBox(color: getRandomColor()),
                          ),
                  ),
                  Positioned(
                    bottom: 8,
                    left: 8,
                    child: FittedBox(
                      fit: BoxFit.scaleDown,
                      alignment: Alignment.centerLeft,
                      child: Container(
                        decoration: BoxDecoration(
                          color: Colors.black.withOpacity(0.4),
                          borderRadius: BorderRadius.circular(8),
                          backgroundBlendMode: BlendMode.darken,
                        ),
                        padding: const EdgeInsets.all(4),
                        child: Text(
                          budget.budgetName,
                          style: Theme.of(context)
                              .textTheme
                              .headlineSmall!
                              .copyWith(color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                  if (usedPercentage >= 100)
                    const Positioned(
                      top: 8,
                      left: 8,
                      child: Icon(
                        Icons.error,
                        color: Colors.red,
                        size: 30,
                      ),
                    )
                  else if (usedPercentage >= 85)
                    const Positioned(
                      top: 8,
                      left: 8,
                      child: Icon(
                        Icons.warning,
                        color: Colors.orange,
                        size: 30,
                      ),
                    ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 8, 8, 4),
            child: DefaultTextStyle(
              softWrap: false,
              overflow: TextOverflow.ellipsis,
              style: Theme.of(context).textTheme.titleMedium!,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 8),
                    child: Text(
                      budget.destination,
                      style: Theme.of(context)
                          .textTheme
                          .titleMedium!
                          .copyWith(color: Colors.white),
                    ),
                  ),
                  Text(
                    budget.startDate.getDateTime().format('MMMM dd, yyyy'),
                    style: const TextStyle(fontSize: 12),
                  ),
                  Text(
                    budget.endDate.getDateTime().format('MMMM dd, yyyy'),
                    style: const TextStyle(fontSize: 12),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
