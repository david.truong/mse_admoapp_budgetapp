import 'package:budget_app/common/ui/bottomsheet_text_form_field.dart';
import 'package:budget_app/common/utils/date_time_formatter.dart';
import 'package:budget_app/features/budgets/controller/budgets_list_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class AddbudgetBottomSheet extends ConsumerStatefulWidget {
  const AddbudgetBottomSheet({
    super.key,
  });

  @override
  AddBudgetBottomSheetState createState() => AddBudgetBottomSheetState();
}

class AddBudgetBottomSheetState extends ConsumerState<AddbudgetBottomSheet> {
  final formGlobalKey = GlobalKey<FormState>();

  final budgetNameController = TextEditingController();
  final destinationController = TextEditingController();
  final limitController = TextEditingController();
  final startDateController = TextEditingController();
  final endDateController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: formGlobalKey,
      child: Container(
        padding: EdgeInsets.only(
          top: 15,
          left: 15,
          right: 15,
          bottom: MediaQuery.of(context).viewInsets.bottom + 15,
        ),
        width: double.infinity,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            BottomSheetTextFormField(
              labelText: AppLocalizations.of(context)!.budgetName,
              controller: budgetNameController,
              keyboardType: TextInputType.text,
            ),
            const SizedBox(
              height: 20,
            ),
            BottomSheetTextFormField(
              labelText: AppLocalizations.of(context)!.budgetDescription,
              controller: destinationController,
              keyboardType: TextInputType.text,
            ),
            const SizedBox(
              height: 20,
            ),
            BottomSheetTextFormField(
              labelText: AppLocalizations.of(context)!.budgetLimit,
              controller: limitController,
              keyboardType: TextInputType.number,
            ),
            const SizedBox(
              height: 20,
            ),
            BottomSheetTextFormField(
              labelText: AppLocalizations.of(context)!.startDate,
              controller: startDateController,
              keyboardType: TextInputType.datetime,
              onTap: () async {
                final pickedDate = await showDatePicker(
                  context: context,
                  initialDate: DateTime.now(),
                  firstDate: DateTime(2000),
                  lastDate: DateTime(2101),
                );
                if (pickedDate != null) {
                  startDateController.text = pickedDate.format('yyyy-MM-dd');
                }
              },
            ),
            const SizedBox(
              height: 20,
            ),
            BottomSheetTextFormField(
              labelText: AppLocalizations.of(context)!.endDate,
              controller: endDateController,
              keyboardType: TextInputType.datetime,
              onTap: () async {
                if (startDateController.text.isNotEmpty) {
                  final pickedDate = await showDatePicker(
                    context: context,
                    initialDate: DateTime.parse(startDateController.text),
                    firstDate: DateTime.parse(startDateController.text),
                    lastDate: DateTime(2101),
                  );

                  if (pickedDate != null) {
                    endDateController.text = pickedDate.format('yyyy-MM-dd');
                  }
                }
              },
            ),
            const SizedBox(
              height: 20,
            ),
            TextButton(
              child: Text(AppLocalizations.of(context)!.validateBudget),
              onPressed: () async {
                final currentState = formGlobalKey.currentState;
                if (currentState == null) {
                  return;
                }
                if (currentState.validate()) {
                  await ref
                      .watch(budgetsListControllerProvider.notifier)
                      .addBudget(
                        name: budgetNameController.text,
                        destination: destinationController.text,
                        limit: int.parse(limitController.text),
                        startDate: startDateController.text,
                        endDate: endDateController.text,
                      );

                  if (context.mounted) {
                    context.pop();
                  }
                }
              },
            ),
          ],
        ),
      ),
    );
  }
}
