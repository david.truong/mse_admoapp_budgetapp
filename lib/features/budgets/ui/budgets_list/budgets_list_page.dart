import 'package:amplify_auth_cognito/amplify_auth_cognito.dart';
import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:budget_app/common/ui/bottom_nav_bar.dart';
import 'package:budget_app/common/ui/top_app_bar.dart';
import 'package:budget_app/common/utils/colors.dart' as constants;
import 'package:budget_app/features/budgets/controller/budgets_list_controller.dart';
import 'package:budget_app/features/budgets/ui/budgets_gridview/budget_list_gridview.dart';
import 'package:budget_app/features/budgets/ui/budgets_list/add_budget_bottomsheet.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:budget_app/common/navigation/router/routes.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class BudgetsListPage extends ConsumerWidget {
  const BudgetsListPage({
    super.key,
  });

  Future<void> showAddBudgetDialog(BuildContext context) =>
      showModalBottomSheet<void>(
        isScrollControlled: true,
        elevation: 5,
        context: context,
        builder: (sheetContext) {
          return const AddbudgetBottomSheet();
        },
      );

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final budgetsListValue = ref.watch(budgetsListControllerProvider);
    return Scaffold(
        appBar: const TopAppBar(),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            showAddBudgetDialog(context);
          },
          backgroundColor: const Color(constants.primaryColorDark),
          child: const Icon(Icons.add),
        ),
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text(
                AppLocalizations.of(context)!.myBudgets,
                style:
                    const TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
              ),
            ),
            const Divider(
              height: 20,
              thickness: 5,
              indent: 20,
              endIndent: 20,
            ),
            const SizedBox(height: 16),
            Expanded(
              child: BudgetsListGridView(
                budgetsList: budgetsListValue,
              ),
            )
          ],
        ),
        bottomNavigationBar:
            BottomNavBar(currentPage: AppRoute.budgetList.name));
  }
}
