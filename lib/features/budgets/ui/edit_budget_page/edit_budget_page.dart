import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:budget_app/common/navigation/router/routes.dart';
import 'package:budget_app/common/ui/bottomsheet_text_form_field.dart';
import 'package:budget_app/common/utils/colors.dart' as constants;
import 'package:budget_app/common/utils/date_time_formatter.dart';
import 'package:budget_app/features/budgets/controller/budget_controller.dart';
import 'package:budget_app/models/ModelProvider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class EditBudgetPage extends ConsumerStatefulWidget {
  const EditBudgetPage({
    required this.budget,
    super.key,
  });

  final Budget budget;

  @override
  EditBudgetPageState createState() => EditBudgetPageState();
}

class EditBudgetPageState extends ConsumerState<EditBudgetPage> {
  @override
  void initState() {
    budgetNameController.text = widget.budget.budgetName;
    destinationController.text = widget.budget.destination;
    limitController.text = widget.budget.limit.toString();

    startDateController.text =
        widget.budget.startDate.getDateTime().format('yyyy-MM-dd');

    endDateController.text =
        widget.budget.endDate.getDateTime().format('yyyy-MM-dd');

    super.initState();
  }

  final formGlobalKey = GlobalKey<FormState>();
  final budgetNameController = TextEditingController();
  final limitController = TextEditingController();
  final destinationController = TextEditingController();
  final startDateController = TextEditingController();

  final endDateController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          AppLocalizations.of(context)!.appTitle,
        ),
        leading: IconButton(
          onPressed: () {
            context.goNamed(
              AppRoute.budget.name,
              pathParameters: {'id': widget.budget.id},
            );
          },
          icon: const Icon(Icons.arrow_back),
        ),
        backgroundColor: const Color(constants.primaryColorDark),
      ),
      body: SingleChildScrollView(
        child: Form(
          key: formGlobalKey,
          child: Container(
            padding: EdgeInsets.only(
              top: 15,
              left: 15,
              right: 15,
              bottom: MediaQuery.of(context).viewInsets.bottom + 15,
            ),
            width: double.infinity,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                BottomSheetTextFormField(
                  labelText: AppLocalizations.of(context)!.budgetName,
                  controller: budgetNameController,
                  keyboardType: TextInputType.text,
                ),
                const SizedBox(
                  height: 20,
                ),
                BottomSheetTextFormField(
                  labelText: AppLocalizations.of(context)!.budgetDescription,
                  controller: destinationController,
                  keyboardType: TextInputType.text,
                ),
                const SizedBox(
                  height: 20,
                ),
                BottomSheetTextFormField(
                  labelText: AppLocalizations.of(context)!.budgetLimit,
                  controller: limitController,
                  keyboardType: TextInputType.number,
                ),
                const SizedBox(
                  height: 20,
                ),
                BottomSheetTextFormField(
                  labelText: AppLocalizations.of(context)!.startDate,
                  controller: startDateController,
                  keyboardType: TextInputType.datetime,
                  onTap: () async {
                    final pickedDate = await showDatePicker(
                      context: context,
                      initialDate: DateTime.now(),
                      firstDate: DateTime(2000),
                      lastDate: DateTime(2101),
                    );

                    if (pickedDate != null) {
                      startDateController.text =
                          pickedDate.format('yyyy-MM-dd');
                    }
                  },
                ),
                const SizedBox(
                  height: 20,
                ),
                BottomSheetTextFormField(
                  labelText: AppLocalizations.of(context)!.endDate,
                  controller: endDateController,
                  keyboardType: TextInputType.datetime,
                  onTap: () async {
                    if (startDateController.text.isNotEmpty) {
                      final pickedDate = await showDatePicker(
                        context: context,
                        initialDate: DateTime.parse(startDateController.text),
                        firstDate: DateTime.parse(startDateController.text),
                        lastDate: DateTime(2101),
                      );

                      if (pickedDate != null) {
                        endDateController.text =
                            pickedDate.format('yyyy-MM-dd');
                      }
                    }
                  },
                ),
                const SizedBox(
                  height: 20,
                ),
                TextButton(
                  child: Text(AppLocalizations.of(context)!.updateBudget),
                  onPressed: () async {
                    final currentState = formGlobalKey.currentState;
                    if (currentState == null) {
                      return;
                    }
                    if (currentState.validate()) {
                      final updatedBudget = widget.budget.copyWith(
                        budgetName: budgetNameController.text,
                        destination: destinationController.text,
                        limit: int.parse(limitController.text),
                        startDate: TemporalDate(
                          DateTime.parse(startDateController.text),
                        ),
                        endDate: TemporalDate(
                          DateTime.parse(endDateController.text),
                        ),
                      );

                      await ref
                          .watch(budgetControllerProvider(widget.budget.id)
                              .notifier)
                          .updateBudget(updatedBudget);
                      if (context.mounted) {
                        context.goNamed(
                          AppRoute.budget.name,
                          pathParameters: {'id': widget.budget.id},
                          extra: updatedBudget,
                        );
                      }
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
