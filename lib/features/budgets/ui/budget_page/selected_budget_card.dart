import 'dart:io';
import 'dart:math';

import 'package:budget_app/common/navigation/router/routes.dart';
import 'package:budget_app/common/ui/upload_progress_dialog.dart';
import 'package:budget_app/common/utils/colors.dart' as constants;
import 'package:budget_app/features/budgets/controller/budget_controller.dart';
import 'package:budget_app/features/budgets/controller/budgets_list_controller.dart';
import 'package:budget_app/features/budgets/ui/budget_page/delete_budget_dialog.dart';
import 'package:budget_app/models/Budget.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:image_picker/image_picker.dart';

class SelectedBudgetCard extends ConsumerWidget {
  const SelectedBudgetCard({
    required this.budget,
    super.key,
  });

  final Budget budget;

  Color getRandomColor() {
    final Random random = Random();
    return Color.fromARGB(
      255,
      random.nextInt(256),
      random.nextInt(256),
      random.nextInt(256),
    );
  }

  Future<bool> uploadImage({
    required BuildContext context,
    required WidgetRef ref,
    required Budget budget,
  }) async {
    final picker = ImagePicker();
    final pickedFile = await picker.pickImage(source: ImageSource.gallery);
    if (pickedFile == null) {
      return false;
    }
    final file = File(pickedFile.path);
    if (context.mounted) {
      showDialog<String>(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return const UploadProgressDialog();
        },
      );

      await ref
          .watch(budgetControllerProvider(budget.id).notifier)
          .uploadFile(file, budget);
    }

    return true;
  }

  Future<bool> deleteBudget(
    BuildContext context,
    WidgetRef ref,
    Budget budget,
  ) async {
    var value = await showDialog<bool>(
      context: context,
      builder: (BuildContext context) {
        return const DeleteBudgetDialog();
      },
    );
    value ??= false;

    if (value) {
      await ref
          .watch(budgetsListControllerProvider.notifier)
          .removeBudget(budget);
    }
    return value;
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    return Card(
      clipBehavior: Clip.antiAlias,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
      elevation: 5,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Text(
            budget.budgetName,
            textAlign: TextAlign.center,
            style: const TextStyle(
              fontSize: 25,
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(
            height: 8,
          ),
          Container(
            alignment: Alignment.center,
            color: const Color(constants.primaryColorDark),
            height: 150,
            child: budget.budgetImageUrl != null
                ? Stack(
                    children: [
                      const Center(child: CircularProgressIndicator()),
                      CachedNetworkImage(
                        cacheKey: budget.budgetImageKey,
                        imageUrl: budget.budgetImageUrl!,
                        width: double.maxFinite,
                        height: 500,
                        alignment: Alignment.center,
                        fit: BoxFit.cover,
                      ),
                    ],
                  )
                : Center(
                    child: ColoredBox(color: getRandomColor()),
                  ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              IconButton(
                onPressed: () {
                  context.goNamed(
                    AppRoute.editBudget.name,
                    pathParameters: {'id': budget.id},
                    extra: budget,
                  );
                },
                icon: const Icon(Icons.edit),
              ),
              IconButton(
                onPressed: () {
                  uploadImage(
                    context: context,
                    budget: budget,
                    ref: ref,
                  ).then((value) {
                    if (value) {
                      Navigator.of(context, rootNavigator: true).pop();
                      ref.invalidate(budgetControllerProvider(budget.id));
                    }
                  });
                },
                icon: const Icon(Icons.camera_enhance_sharp),
              ),
              IconButton(
                onPressed: () {
                  deleteBudget(context, ref, budget).then((value) {
                    if (value) {
                      context.goNamed(
                        AppRoute.home.name,
                      );
                    }
                  });
                },
                icon: const Icon(Icons.delete),
              ),
            ],
          )
        ],
      ),
    );
  }
}
