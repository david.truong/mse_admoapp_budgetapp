import 'package:budget_app/common/navigation/router/routes.dart';
import 'package:budget_app/common/utils/colors.dart' as constants;
import 'package:budget_app/features/budgets/controller/budget_controller.dart';
import 'package:budget_app/features/budgets/ui/budget_page/budget_details.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:go_router/go_router.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class BudgetPage extends ConsumerWidget {
  const BudgetPage({
    required this.budgetId,
    super.key,
  });
  final String budgetId;

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final budgetValue = ref.watch(budgetControllerProvider(budgetId));

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(
          AppLocalizations.of(context)!.appTitle,
        ),
        leading: IconButton(
          onPressed: () {
            context.goNamed(
              AppRoute.budgetList.name,
            );
          },
          icon: const Icon(Icons.arrow_back),
        ),
        backgroundColor: const Color(constants.primaryColorDark),
      ),
      body: BudgetDetails(
        budgetId: budgetId,
        budget: budgetValue,
      ),
    );
  }
}
