import 'package:budget_app/features/budgets/controller/budget_controller.dart';
import 'package:budget_app/features/budgets/ui/budget_page/selected_budget_card.dart';
import 'package:budget_app/models/ModelProvider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:budget_app/features/transactions/controller/transactions_of_budget_list_controller.dart';
import 'package:budget_app/features/transactions/ui/transaction_card/transaction_list_card.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class BudgetDetails extends ConsumerWidget {
  const BudgetDetails({
    required this.budget,
    required this.budgetId,
    super.key,
  });

  final AsyncValue<Budget> budget;
  final String budgetId;

  double calculateTotalSpent(List<Transaction> transactions, Budget budget) {
    return transactions
        .where((transaction) => transaction.budgetID == budget.id)
        .fold(0.0, (sum, transaction) => sum + transaction.amount);
  }

  double calculateUsedPercentage(double totalSpent, Budget budget) {
    return (totalSpent / budget.limit!) * 100;
  }

  @override
  Widget build(BuildContext context, WidgetRef ref) {
    final transactionListValue =
        ref.watch(transactionsOfBudgetsListControllerProvider(budgetId));
    final filteredTransactions = transactionListValue.when(
      data: (transactions) => transactions.toList(),
      loading: () => <Transaction>[],
      error: (error, stackTrace) => <Transaction>[],
    );

    switch (budget) {
      case AsyncData(:final value):
        final double totalSpent =
            calculateTotalSpent(filteredTransactions, value);
        final double usedPercentage =
            calculateUsedPercentage(totalSpent, value);

        return Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            const SizedBox(
              height: 8,
            ),
            Stack(
              children: [
                SelectedBudgetCard(budget: value),
                if (usedPercentage >= 100)
                  const Positioned(
                    top: 8,
                    left: 8,
                    child: Icon(
                      Icons.error,
                      color: Colors.red,
                      size: 30,
                    ),
                  )
                else if (usedPercentage >= 85)
                  const Positioned(
                    top: 8,
                    left: 8,
                    child: Icon(
                      Icons.warning,
                      color: Colors.orange,
                      size: 30,
                    ),
                  ),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '${AppLocalizations.of(context)!.totalAmount}: ${totalSpent.toStringAsFixed(2)} CHF',
                    style: const TextStyle(fontSize: 16),
                  ),
                  Text(
                    '${AppLocalizations.of(context)!.budgetLimit}: ${value.limit!.toStringAsFixed(2)} CHF',
                    style: const TextStyle(fontSize: 16),
                  ),
                  Text(
                    '${AppLocalizations.of(context)!.usedPercentage}: ${usedPercentage.toStringAsFixed(0)}%',
                    style: const TextStyle(fontSize: 16),
                  ),
                ],
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            const Divider(
              height: 20,
              thickness: 5,
              indent: 20,
              endIndent: 20,
            ),
            Text(
              AppLocalizations.of(context)!.myTransactions,
              textAlign: TextAlign.center,
              style: const TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.bold,
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            Expanded(
              child: TransactionListCard(transactionList: filteredTransactions),
            )
          ],
        );

      case AsyncError():
        return Center(
          child: Column(
            children: [
              Text(AppLocalizations.of(context)!.error),
              TextButton(
                onPressed: () async {
                  ref.invalidate(budgetControllerProvider(budgetId));
                },
                child: Text(AppLocalizations.of(context)!.tryAgain),
              ),
            ],
          ),
        );
      case AsyncLoading():
        return const Center(
          child: CircularProgressIndicator(),
        );

      case _:
        return Center(
          child: Text(AppLocalizations.of(context)!.error),
        );
    }
  }
}
