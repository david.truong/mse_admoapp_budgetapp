import 'package:amplify_auth_cognito/amplify_auth_cognito.dart';
import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:budget_app/common/utils/colors.dart' as constants;

class TopAppBar extends StatelessWidget implements PreferredSizeWidget {
  const TopAppBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      centerTitle: true,
      title: Text(
        AppLocalizations.of(context)!.appTitle,
      ),
      backgroundColor: const Color(constants.primaryColorDark),
      actions: [
        IconButton(
          icon: const Icon(Icons.logout),
          tooltip: 'Logout',
          onPressed: () async {
            final result = await Amplify.Auth.signOut();
            if (result is CognitoCompleteSignOut) {
              safePrint('Sign out completed successfully');
            } else if (result is CognitoFailedSignOut) {
              safePrint('Error signing user out: ${result.exception.message}');
            }
          },
        ),
      ],
    );
  }

  @override
  Size get preferredSize => const Size.fromHeight(kToolbarHeight);
}
