import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:budget_app/common/navigation/router/routes.dart';
import 'package:budget_app/common/utils/colors.dart' as constants;

class BottomNavBar extends StatelessWidget {
  final String currentPage;

  const BottomNavBar({Key? key, required this.currentPage}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      color: const Color(constants.primaryColorDark),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          IconButton(
            onPressed: () {
              if (currentPage != AppRoute.home.name) {
                context.goNamed(AppRoute.home.name);
              }
            },
            icon: Icon(
              Icons.home,
              color: currentPage == AppRoute.home.name
                  ? Colors.white
                  : Colors.grey,
            ),
          ),
          IconButton(
            onPressed: () {
              if (currentPage != AppRoute.budgetList.name) {
                context.goNamed(AppRoute.budgetList.name);
              }
            },
            icon: Icon(
              Icons.list,
              color: currentPage == AppRoute.budgetList.name
                  ? Colors.white
                  : Colors.grey,
            ),
          ),
          IconButton(
            onPressed: () {
              if (currentPage != AppRoute.transactionList.name) {
                context.goNamed(AppRoute.transactionList.name);
              }
            },
            icon: Icon(
              Icons.account_balance_wallet,
              color: currentPage == AppRoute.transactionList.name
                  ? Colors.white
                  : Colors.grey,
            ),
          ),
        ],
      ),
    );
  }
}
