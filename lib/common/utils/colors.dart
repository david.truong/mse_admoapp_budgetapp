import 'package:flutter/material.dart';

const Map<int, Color> primarySwatch = {
  50: Color.fromRGBO(71, 94, 226, .1),
  100: Color.fromRGBO(71, 94, 226, .2),
  200: Color.fromRGBO(71, 94, 226, .3),
  300: Color.fromRGBO(71, 94, 226, .4),
  400: Color.fromRGBO(71, 94, 226, .5),
  500: Color.fromRGBO(71, 94, 226, .6),
  600: Color.fromRGBO(71, 94, 226, .7),
  700: Color.fromRGBO(71, 94, 226, .8),
  800: Color.fromRGBO(71, 94, 226, .9),
  900: Color.fromRGBO(71, 94, 226, 1),
};
const MaterialColor primaryColor = MaterialColor(0xFF4D44FF, primarySwatch);
const int primaryColorDark = 0xFF4D44FF;
