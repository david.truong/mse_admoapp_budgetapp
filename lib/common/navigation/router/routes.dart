enum AppRoute {
  home,
  budgetList,
  budget,
  editBudget,
  transactionList,
  transaction,
  editTransaction,
}
