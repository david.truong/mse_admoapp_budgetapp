import 'package:budget_app/common/navigation/router/routes.dart';
import 'package:budget_app/features/budgets/ui/edit_budget_page/edit_budget_page.dart';
import 'package:budget_app/features/budgets/ui/budget_page/budget_page.dart';
import 'package:budget_app/features/budgets/ui/budgets_list/budgets_list_page.dart';
import 'package:budget_app/features/home_page/ui/home_page.dart';
import 'package:budget_app/features/transactions/ui/edit_transaction_page/edit_transaction_page.dart';
import 'package:budget_app/features/transactions/ui/transactions_list/transactions_list_page.dart';
import 'package:budget_app/models/ModelProvider.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class NoTransitionPage<T> extends CustomTransitionPage<T> {
  NoTransitionPage({
    required super.child,
  }) : super(
          transitionDuration: Duration.zero,
          reverseTransitionDuration: Duration.zero,
          transitionsBuilder: (_, __, ___, child) => child,
        );
}

final router = GoRouter(
  routes: [
    GoRoute(
      path: '/',
      name: AppRoute.home.name,
      pageBuilder: (context, state) {
        return NoTransitionPage(child: const HomePage());
      },
    ),
    GoRoute(
      path: '/budgets',
      name: AppRoute.budgetList.name,
      pageBuilder: (context, state) {
        return NoTransitionPage(child: const BudgetsListPage());
      },
    ),
    GoRoute(
      path: '/budget/:id',
      name: AppRoute.budget.name,
      builder: (context, state) {
        final budgetId = state.pathParameters['id']!;
        return BudgetPage(budgetId: budgetId);
      },
    ),
    GoRoute(
      path: '/budget/:id/edit',
      name: AppRoute.editBudget.name,
      pageBuilder: (context, state) {
        return NoTransitionPage(
          child: EditBudgetPage(
            budget: state.extra! as Budget,
          ),
        );
      },
    ),
    GoRoute(
        path: '/transactions',
        name: AppRoute.transactionList.name,
        pageBuilder: (context, state) {
          return NoTransitionPage(child: const TransactionsListPage());
        }),
    GoRoute(
      path: '/transactions/:id/edit',
      name: AppRoute.editTransaction.name,
      builder: (context, state) {
        final transaction = state.extra! as Transaction;
        return EditTransactionPage(transaction: transaction);
      },
    )
  ],
  errorBuilder: (context, state) => Scaffold(
    body: Center(
      child: Text(state.error.toString()),
    ),
  ),
);
